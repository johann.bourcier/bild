## Overview

This Python application organizes the media files (pictures and videos) in directories by date.

This application is written using Python 3.6 and later.

## Motivation

I really like being able to easily organize the pictures and videos from multiple devices (phones, cameras, tablets)
into a single set of folders organized by date.

## How to Run

In the top-level directory:

    $ python run.py

This project uses Python 3.9.

## Configuration

There are (3) key parameters that should be defined in run.py:

| Parameter                       | Description                                                |
| ------------------------------- | ---------------------------------------------------------- |
| SOURCE_DIRECTORY                | Directory containing media files (such as from an iPhone)  |
| DESTINATION_DIRECTORY_PICTURES  | Directory where picture files should be stored             |
| DESTINATION_DIRECTORY_VIDEOS    | Directory where video files should be stored               |

## Testing

    $ pytest

## What is run in CI?

The following checks are run in GitLab CI (Continuous Integration) to perform checks on the source code:

1. `flake8` - static analysis
1. `pylint` - static analysis
1. `mypy` - type checking
1. `isort` - formatting of `import` statements
1. `safety` - security check on dependent packages
1. `pytest` - testing
1. `coverage` - test coverage of source code

## Static Analysis (Manual)

To run `bandit` to check for security vulnerabilites:

    $ bandit bild/*.py

NOTE: `bandit` reports a security vulnerability in the used of `subprocess.run()` in *bild/file.py*. However,
      the input being passed to `subprocess.run()` is being sanitized using `shlex.quote()` to escape the
      input.  For more details: https://docs.python.org/3/library/subprocess.html#security-considerations
